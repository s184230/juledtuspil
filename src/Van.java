import javafx.scene.Node;
import javafx.scene.image.Image;

public class Van extends Game {
    public static final String VAN_IMAGE_LOCATION =
            "Pics/TuborgBil.png";
    public static Image vanImage;
    public static Node van;
    static boolean goWest;
    static boolean running;
    static boolean goNorth;
    static boolean goSouth;
    static boolean goEast;
    static boolean shooting;

    static void moveVanBy(Game game, int dx, int dy) {
        if (dx == 0 && dy == 0) return;

        final double cx = van.getBoundsInLocal().getWidth() / 2;
        final double cy = van.getBoundsInLocal().getHeight() / 2;

        double x = cx + van.getLayoutX() + dx;
        double y = cy + van.getLayoutY() + dy;

        Van.moveVanTo(x, y);
    }

    static void moveVanTo(double x, double y) {
        final double cx = van.getBoundsInLocal().getWidth() / 2;
        final double cy = van.getBoundsInLocal().getHeight() / 2;

        if (x - cx >= 0 &&
                x + cx <= Game.WIDTH_CANVAS &&
                y - cy >= 0 &&
                y + cy <= Game.HEIGHT_CANVAS) {
            van.relocate(x - cx, y - cy);
        }
    }
}