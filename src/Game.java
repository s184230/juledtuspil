import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.StrokeType;
import javafx.stage.Stage;

import static javafx.util.Duration.millis;

/**
 * Hold down an arrow key to have your hero move around the screen.
 * Hold down the shift key to have the hero run.
 */
public class Game extends Application {

    public static final double WIDTH_CANVAS = 1280, HEIGHT_CANVAS = 720;

    /**
     * Denne klasse får bilen til at skyde.
     */
    static void shoot() {
        ((ImageView) Projectile.projectile).setFitHeight(100);
        ((ImageView) Projectile.projectile).setFitWidth(50);
        Projectile.projectile.setRotate(90);
        Projectile.projectile.relocate(Van.van.getLayoutX(), Van.van.getLayoutY());
        final Timeline shootingAnimation = new Timeline();
        shootingAnimation.setCycleCount(1);
        shootingAnimation.setAutoReverse(false);
        final KeyValue keyValue = new KeyValue(Projectile.projectile.layoutXProperty(), 1500);
        final KeyFrame keyFrame = new KeyFrame(millis(600), keyValue);
        shootingAnimation.getKeyFrames().add(keyFrame);
        shootingAnimation.play();
    }


    @Override
    public void start(Stage stage) throws Exception {
        Group root = new Group();

        Van.vanImage = new Image(Van.VAN_IMAGE_LOCATION);
        Van.van = new ImageView(Van.vanImage);

        ((ImageView) Van.van).setFitHeight(100);
        ((ImageView) Van.van).setFitWidth(200);

        Group projectiles = new Group();
        Projectile.projectileImage = new Image(Projectile.PROJECTILE_IMAGE_LOCATION);
        Projectile.projectile = new ImageView(Projectile.projectileImage);
        Circle circle = new Circle(150, Color.web("white", 0.05));
        circle.setStrokeType(StrokeType.OUTSIDE);
        circle.setStroke(Color.web("white", 0.16));
        circle.setStrokeWidth(4);
        circle.setCenterX(30);
        projectiles.getChildren().add(Projectile.projectile);

        root.getChildren().add(projectiles);

        Group landScape = new Group(Van.van, Projectile.projectile, root);

        Van.moveVanTo(105, HEIGHT_CANVAS / 2);

        Scene scene = new Scene(landScape, WIDTH_CANVAS, HEIGHT_CANVAS, Color.NAVY);

        scene.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case UP:
                    Van.goNorth = true;
                    break;
                case DOWN:
                    Van.goSouth = true;
                    break;
                case LEFT:
                    Van.goWest = true;
                    break;
                case RIGHT:
                    Van.goEast = true;
                    break;
                case SHIFT:
                    Van.running = true;
                    break;
                case SPACE:
                    Van.shooting = true;
                    break;
            }
        });

        scene.setOnKeyReleased(event -> {
            switch (event.getCode()) {
                case UP:
                    Van.goNorth = false;
                    break;
                case DOWN:
                    Van.goSouth = false;
                    break;
                case LEFT:
                    Van.goWest = false;
                    break;
                case RIGHT:
                    Van.goEast = false;
                    break;
                case SHIFT:
                    Van.running = false;
                    break;
                case SPACE:
                    Van.shooting = false;
                    break;
            }
        });

        stage.setScene(scene);
        stage.show();

        AnimationTimer animationTimer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                int deltaX = 0, deltaY = 0;

                if (Van.goNorth) deltaY -= 3;
                if (Van.goSouth) deltaY += 3;
                if (Van.goEast) deltaX += 0;
                if (Van.goWest) deltaX -= 0;
                if (Van.running) {
                    deltaX *= 5;
                    deltaY *= 5;
                }
                if (Van.shooting) {
                    shoot();
                }

                Van.moveVanBy(Game.this, deltaX, deltaY);
            }
        };
        animationTimer.start();
    }

    public static void main(String[] args) {
        launch(args);
    }
}